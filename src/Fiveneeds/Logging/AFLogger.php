<?php
/**
 * Created by PhpStorm.
 * User: shuichi.suzuki
 * Date: 2019/03/14
 * Time: 15:29
 */

namespace Fiveneeds\Logging;


use Illuminate\Support\Facades\Log;

class AFLogger
{
    private static $time=0;

    /**
     * @param mixed $content string, array stdClass or any json encodable object
     * @param mixed $context string or array
     */
    public static function debug($content,$context=[]):void {
        AFLogger::log($content,$context,__FUNCTION__);
    }

    /**
     * @param mixed $content string, array stdClass or any json encodable object
     * @param mixed $context string or array
     */
    public static function info($content,$context=[]):void {
        AFLogger::log($content,$context,__FUNCTION__);
    }

    /**
     * @param mixed $content string, array stdClass or any json encodable object
     * @param mixed $context string or array
     */
    public static function error($content,$context=[]):void {
        AFLogger::log($content,$context,__FUNCTION__);
    }

    /**
     * @param mixed $content string, array stdClass or any json encodable object
     * @param mixed $context string or array
     */
    public static function warn($content,$context=[]):void {
        AFLogger::log($content,$context,__FUNCTION__);
    }

    /**
     * @param mixed $content string, array stdClass or any json encodable object
     * @param mixed $context string or array
     */
    public static function critical($content,$context=[]):void {
        AFLogger::log($content,$context,__FUNCTION__);
    }

    /**
     * @param mixed $content string, array stdClass or any json encodable object
     * @param mixed $context string or array
     * @param string $log_level
     */
    private static function log($content,$context,string $log_level="debug"){
        if(is_string($context)){
            $context=["tag"=>$context];
        }
        if( AFLogger::$time==0){
            AFLogger::$time = $_SERVER['REQUEST_TIME_FLOAT'];
        }
        $now =  microtime(true);
        $time_consumed = $now - AFLogger::$time;
        AFLogger::$time = $now;

        $context["time_consumed"]=$time_consumed;
        $context["request_time_consumed"]=$now - $_SERVER['REQUEST_TIME_FLOAT'];
        Log::channel(env("AF_LOG_CHANNEL","fn"))->log($log_level,json_encode($content, JSON_UNESCAPED_UNICODE),$context);
    }
}
