<?php

namespace Fiveneeds\Logging;

use Monolog\Formatter\FormatterInterface;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\HandlerInterface;
use Monolog\Logger;
use Monolog\Processor\IntrospectionProcessor;
use Monolog\Processor\WebProcessor;
use Monolog\Processor\ProcessIdProcessor;

class AFFormatter
{
    private $logFormat = '{"level_name":"%level_name%","level":%level%,  "context":%context%, "extra":{"url":"%extra.url%","server_ip":"%extra.server_ip%","client_ip":"%extra.client_ip%","request_time":"%extra.request_time%","pip":"%extra.process_id%"},  "source":{ "file":"%extra.file%", "line":%extra.line%, "function":"%extra.function%"} , "data":%message%, "timestamp":"%datetime%"}' . PHP_EOL;

    public function __invoke($monolog)
    {
        $formatter = new LineFormatter($this->logFormat);
        $ip = new IntrospectionProcessor(Logger::DEBUG, ['Illuminate\\', 'Fiveneeds\\Logging\\']);

        $extraFields = array(
            'url' => 'REQUEST_URI',
            'server_ip' => 'SERVER_ADDR',
            'client_ip' => 'HTTP_X-Forwarded-For',
            'request_time' => 'REQUEST_TIME_FLOAT',
        );

        $wp = new WebProcessor(null, $extraFields);
        $pid = new ProcessIdProcessor();

        $monolog->useMicrosecondTimestamps(true);


        foreach ($monolog->getHandlers() as $handler) {
            $handler->setFormatter($formatter);
            $handler->pushProcessor($ip);
            $handler->pushProcessor($wp);
            $handler->pushProcessor($pid);
        }
    }
}
